<?php
	$cMetaDesc = '';
	$cMetaKW = '';
	$cPageTitle = 'Home';
	$cSEOTitle = '';
	$layout = 'home';
?>

<?php
	include("header.php");
?>
<!--EVENT BANNER START-->
<div class="event-banner">
	<div class="row">
		<!--EVENT BANNER LEFT IMAGE-->
		<div class="large-8 columns small-6 small-centered large-uncentered">
			<a href="index.php"><img src="img/honda-indy-logo.png" alt="honda-indy-logo" width="115" height="146" /></a>
		</div>
		
		<!--EVENT BANNER INFO RIGHT-->
		<div class="large-4 columns">
			<h3>April 25 - 27, 2014</h3>
			<h4>Barber Motorsports Park<br>
				Birmingham, Alabama
			</h4>
		</div>
	</div>
</div>

<!--START SECOND NAV-->
<div class="contain-to-grid">
<div class="event-nav">
	<nav class="top-bar" data-topbar>
	  <ul class="title-area">
	    <li class="name">
	      <h1><a href="#"></a></h1>
	    </li>
	    <li class="toggle-topbar menu-icon"><a href="#">Menu</a></li>
	  </ul>
	
	  <section class="top-bar-section">
	    <ul>
	      <li><a href="index.php">Home</a></li>
	      <li class="has-dropdown"><a href="#">Event Info</a>
	      	<ul class="dropdown">
		      	<li><a href="#">Event Schedule</a></li>
		      	<li><a href="spectator-info.php">Spectator Info</a></li>
		      	<li><a href="#">Directions and Parking</a></li>
		      	<li><a href="#">Event Map</a></li>
		      	<li><a href="#">Honda Grand Prix of AL</a></li>
	      	</ul>
	      </li>
	      <li><a href="#">Tickets</a></li>
	      <li><a href="#">Hospitality</a></li>
	      <li><a href="#">Multimedia</a></li>
	      <li><a href="#">Press</a></li>
	      <li><a href="#">Volunteer</a></li>
	      <li><a href="#">Contact Us</a></li>
	    </ul>
	    
	  </section>
	</nav>
	</div>
</div>

<!--SLIDER WRAPPER START-->
<!--
<div class="slider-wrapper">
	<div class="countdown-toggle hide-for-small">
		<span class="countdown-toggle-text"><h3>COUNTDOWN BY</h3></span>
		<div class="countdown-sponsor-logo">
			<img src="img/countdown-sponsor.png" alt="countdown-sponsor" />
		</div>
			
			<div id="countdown" class="countdownHolder"></div>
			<div class="row">
				<div class="large-4 columns"><p>Days</p></div>
				<div class="large-4 columns"><p>Hours</p></div>
				<div class="large-4 columns"><p>Minutes</p></div>
			</div>
	</div>
	
</div>
-->


<!--CONTENT START-->
<div class="page-wrapper">
	<div class="row">
		<!--START CONTENT TEXT-->
		<div class="large-8 columns">
			<h1>Spectator Info</h1>
			<p><h2>ADA ACCESSIBILITY</h2><br>
ADA accessible viewing is located next to Turn 14 and adjacent to Lot E. Spectators needing assistance can ride one of the trams to move throughout the park. These trams will run continuously throughout each day of the race weekend.<br><br>

 
<h2>ADA PARKING</h2><br>
ADA parking is located on site at Turn 14, in Lot D and adjacent to the ADA viewing area in Lot E.<br><br>

 
<h2>AIRPORT</h2><br>
Birmingham-Shuttlesworth International Airport - <a href="#">MORE INFORMATION</a><br><br>

 
<h2>ALCOHOL POLICY</h2><br>
No alcoholic beverages may be served or sold to anyone under the age of twenty-one (21) years. There shall be no consumption of alcohol on the shuttles from any of the parking lots.<br><br>

 
<h2>ATM</h2><br>
There will be one (1) ATM machine located in the fan zone.<br><br>
 
 
 
<h2>CAMERA POLICY</h2><br>
Cameras are permitted for general spectator use throughout the weekend.<br><br>

<h2>CONCESSIONS</h2><br><br>
Concessions will be located in the Fan Zone, turn 2 and 3, turn 15 and 16, and the paddock.<br><br>


<h2>COOLER POLICY</h2><br>
Spectators are allowed to bring coolers on-site. All coolers are subject to reasonable inspection before entering and/or at anytime throughout the race weekend. There shall be no consumption of alcohol on the shuttles.<br><br>

 
<h2>DROP-OFF & PICK-UP</h2><br>
Friday & Saturday drop-off and pick-up will be at the Credential Building located right outside Gate 2.<br>
Sunday drop-off and pick-up will be located at The Outlet Shops of  Grand River.<br><br>
 
 
<h2>EAR PROTECTION</h2><br>
For your safety, comfort, and viewing pleasure, ear protection is advised for all spectators. To purchase yours, please visit our official Event Merchandise vendors located throughout Barber Motorsports Park.<br><br>

 

<h2>FIRST AID LOCATIONS</h2><br>
Spectators in need of medical attention should visit the First Aid location. Additionally, there will be Birmingham Fire and Rescue units staged at the Turn 2/3 viewing area, the back straight viewing area, and the Turn 15/16 viewing area. There is also a Birmingham Fire & Rescue location in the Paddock by Gate 17.<br><br>

 

<h2>KIDS TICKETS</h2><br>
Kids 12 and under are admitted free to the races when accompanied by a ticketed adult each day of the race weekend.<br><br>
 

 
<h2>PARK RULES</h2><br>
Helmets must be worn on Perimeter Road at all times when operating any motorized vehicle with handle bars.<br>
All vehicles operating on Perimeter Road must be street legal. (vintage motorcycle exception).<br>
Skateboards, roller blades, roller skates, razor scooters or any similar type item are not allowed anywhere in the park.<br>
ATVs such as 4-wheelers, mules, etc. are not allowed anywhere in the park.<br>
Posted speed limits must be obeyed. 15 MPH is maximum.<br>
Bicycles are allowed in the park. Minors operating bicycles must be accompanied by an adult at all times.<br>
No pets are allowed on the facility grounds. Animals required to assist handicapped individuals are allowed throughout the park.<br>
No abusive behavior or vandalism. Either could result in ejection/arrest or both.<br>
No fireworks, firearms or weapons are permitted on facility grounds.<br>
No flyers or handbills may be distributed unless prior approval is obtained from management.<br>
No glass containers are allowed on park property at any time.<br>
No stakes of any kind may be driven into paved surfaces.<br>
No loud music.<br>
Do not damage grass areas.<br>
No campfire or ground fires are allowed on park property. Grills are only allowed in camping areas.<br>
No unlawful activities. Federal, State, and local laws must be obeyed.<br>
No entrance into restricted areas without proper credentials.<br>
No jumping fences.<br>
Visitors are not allowed to rope off any areas. Scaffolding is prohibited.<br>
All trash must be placed into proper receptacles.<br>
Any other items deemed unlawful or dangerous by Event and/or Series Security Personnel in their sole discretion. Parents with small children are allowed to bring strollers and diaper bags, if needed. These items are subject to reasonable inspection before entering and/or at any time during the race weekend.<br>
Umbrella's are allowed, just please be respectful not to block the view of those behind you.<br>

<h2>PARKING INFORMATION</h2><br>
Friday and Saturday parking is free onsite at the Barber Motorsports Park. Sunday onsite parking is available for purchase for both cars and motorcycles. All other spectators will receive complimentary offsite parking at The Outlet Shops of Grand River with a brief shuttle to the Park.<br><br>

 

<h2>RADIO</h2><br>
Bring your personal headphones to hear all on-track action broadcasted on 106.3 FM.<br><br>

 

<h2>RAIN POLICY</h2><br>
If it rains on any day of the race weekend, the races will continue as long as it is deemed safe to do so. All Corporate Hospitality will be open every day rain or shine. The sanctioning bodies and event staff will always keep spectators informed at the earliest possible moment of any changes made on this policy based upon situational circumstances.<br><br>


<h2>RESTROOMS</h2><br>
Restrooms are located throughout the park, mostly adjacent to viewing and concession areas.<br><br>

 

<h2>TICKETS</h2><br>
A variety of ticket options are available for purchase online to be printed instantly or at the main gate.<br><br>
 

<h2>WILL CALL</h2><br>
Will Call will be located at the Hampton Inn.<br><br>
 

<h2>WEATHER WARNING</h2><br>
For the most up to date information, please tune your radio to 106.3 FM
If inclement weather is approaching, special PA announcements will be made continuously during the period of potential inclement weather. Spectators will be made aware of the impending weather or advised to take immediate precautions prior to the race event being suspended. The following is a list of weather warning scenarios:<br>
Scenario 1: Rain showers in the vicinity: Spectators will be alerted that rain showers are in the area, but racing will continue.<br>
Scenario 2: Severe Weather Warning: Spectators will be alerted that severe weather is in the area. Racing will continue until officials determine that the safety of the competitors and spectators is at risk. In the event of a severe weather warning, please take shelter in the following locations:<br>
In your vehicle<br>
Barber Vintage Motorsports Museum<br>
Race Control and Shower Tower - Available in the Paddock<br>
Permanent Buildings in Lot E -<br>
PA announcements will be made continuously during the period of inclement weather.<br>

Racing will continue promptly after the storm clears.</p>
		</div>
		<div class="large-4 columns">
			
		</div>
	</div>
</div>

<!--START PANELS-->
<div class="panel-wrapper">
	<div class="row">
		<!--START SPONSOR PANEL-->
		<div class="large-4 columns">
			<div class="sponsor-panel">
				<h2>2014 Sponsor</h2>	
				<div class="inside-panel">
					<a href="#"><img src="img/countdown-sponsor.png" alt="countdown-sponsor" width="279" height="96" /></a>
				</div>
			</div>
		</div>
		
		<!--START NEWSLETTER PANEL-->
		<div class="large-4 columns">
			<div class="newsletter-panel">
				<h2>Newsletter</h2>
				<div class="inside-panel">
					<!--START FORM-->
					<form>
					  <div class="row">
					    <div class="large-12 columns">
					      <div class="row collapse">
					      	<label>Sign Up for Our Newsletter!</label>
					        <div class="large-12 columns">
								<input type="text" placeholder="Email Address">
					        </div>
					      </div>
					    </div>
					  </div>
					  <div class="row">
						  <div class="large-6 columns large-centered">
							  <a class="button expand" href="#">Sign Up</a>
						  </div>
					  </div>
					</form>
				</div>
			</div>
		</div>
		
		<!--START SOCIAL MEDIA PANEL-->
		<div class="large-4 columns">
			<div class="social-panel">
				<h2>Social Media</h2>
				<div class="inside-panel">
					<p>CONNECT WITH US!</p>
					<ul class="inline-list">
						<li><a href="#">
							<img src="img/twitter.png" alt="twitter" />
						</a></li>
						<li><a href="#">
							<img src="img/facebook.png" alt="twitter" />
						</a></li>
						<li><a href="#">
							<img src="img/youtube.png" alt="twitter" />
						</a></li>
						<li><a href="#">
							<img src="img/instagram.png" alt="twitter" />
						</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
	include("footer.php");
?>