<?php
	$cMetaDesc = '';
	$cMetaKW = '';
	$cPageTitle = 'Home';
	$cSEOTitle = '';
	$layout = 'home';
?>

<?php
	include("header.php");
?>
<!--EVENT BANNER START-->
<div class="event-banner">
	<div class="row">
		<!--EVENT BANNER LEFT IMAGE-->
		<div class="medium-7 columns small-6 large-uncentered medium-uncentered">
			<a href="index.php"><img class="event-logo" src="img/honda-indy-logo.png" alt="honda-indy-logo" width="115" height="146" /></a>
		</div>
		
		<!--EVENT BANNER INFO RIGHT-->
		<div class="medium-5 columns  small-6  large-uncentered  medium-uncentered">
			<h3>April 25 - 27, 2014</h3>
			<h4>Barber Motorsports Park<br>
				<span>Birmingham, Alabama</span>
			</h4>
		</div>
	</div>
</div>

<!--START SECOND NAV-->
<div class="contain-to-grid event-menu">
<div class="event-nav">
	<nav class="top-bar" data-topbar>
	  <ul class="title-area">
	    <li class="name"><!--Leave Empty--></li>
	    <li class="toggle-topbar menu-icon"><a href="#">Menu</a></li>
	  </ul>
	
	  <section class="top-bar-section">
	    <ul>
	    	
        <li class="hide-for-medium-only"><a href="index.php">Home</a></li>
	    
	      <li class="has-dropdown"><a href="#">Event Info</a>
	      
	      	<ul class="dropdown">
		      	<li><a href="#">Event Schedule</a></li>
		      	<li><a href="spectator-info.php">Spectator Info</a></li>
		      	<li><a href="#">Directions and Parking</a></li>
		      	<li><a href="#">Event Map</a></li>
		      	<li><a href="#">Honda Grand Prix of AL</a></li>
	      	</ul>
	      </li>
	       
	      <li class="has-dropdown"><a href="#">Tickets</a>
	        <ul class="dropdown">
		      	<li><a href="#">Purchase General Admission Tickets</a></li>
		      	<li><a href="#">Purchase Camping Passes</a></li>
		      	<li><a href="#">Purchase CM's Trackside Tavern Tix</a></li>
		      	<li><a href="#">Purchase Firestone Car Corral Passes</a></li>
	      	</ul>
	      </li>
	       
	      <li class="has-dropdown"><a href="#">Hospitality</a>
	        <ul class="dropdown">
		      	<li><a href="#">CM's Trackside Tavern</a></li>
		      	<li><a href="#">Stella Artois Paddock Club</a></li>
		      	<li><a href="#">Corporate Skyboxes &amp; Tents</a></li>
		      	<li><a href="#">Additional Sponsorship Info</a></li>
	      	</ul>
	      </li>
	       
	      <li class="has-dropdown"><a href="#">Multimedia</a>
	        <ul class="dropdown">
		      	<li><a href="#">In the News</a></li>
		      	<li><a href="#">Photo Gallery</a></li>
		      	<li><a href="#">Video Gallery</a></li>
		      	<li><a href="#">Social Media</a></li>
	      	</ul>
	      </li>
	
	      <li class="has-dropdown"><a href="#">Press</a>
	        <ul class="dropdown">
		      	<li><a href="#">Press Releases</a></li>
		      	<li><a href="#">Media Request</a></li>
	      	</ul>
	      </li>
	 
	      <li class="has-dropdown"><a href="#">Volunteer</a>
	        <ul class="dropdown">
		      	<li><a href="#">Apply Today</a></li>
		      	<li><a href="#">Volunteer Login</a></li>
		      	<li><a href="#">Volunteer Info</a></li>
		      	<li><a href="#">Committee Descriptions</a></li>
		      	<li><a href="#">Volunteer FAQ</a></li>
	      	</ul>
	      </li>
	      
	      <li><a href="#">Contact Us</a></li>
	    </ul>
	    
	  </section>
	</nav>
	</div>
</div>

<!--SLIDER WRAPPER START-->
<div class="slider-wrapper">
	<div class="countdown-toggle show-for-large-up">
		<span class="countdown-toggle-text"><h3>COUNTDOWN BY</h3></span>
		<div class="countdown-sponsor-logo">
			<img src="img/countdown-sponsor.png" alt="countdown-sponsor" />
		</div>
			
			<div id="countdown" class="countdownHolder"></div>
			<div class="row">
				<div class="small-4 columns"><p>Days</p></div>
				<div class="small-4 columns"><p>Hours</p></div>
				<div class="small-4 columns"><p>Minutes</p></div>
			</div>
	</div>
	<ul class="example-orbit" data-orbit data-options="animation:fade; slide_number:false; bullets:false; pause_on_hover:false;">
		<li>
			<img src="img/slider-1.jpg" alt="slider-1" />
      <!-- <div class="orbit-caption">
        Headline
      </div> -->
		
		</li>
		<li>
			<img src="img/slider-2.jpg" alt="slider-2" />
		
		</li>
		<li>
			<img src="img/slider-3.jpg" alt="slider-3" />
		
		</li>
	</ul>
</div>


<!--START PANELS-->
<div class="panel-wrapper">
	<div class="row">
		<!--START SPONSOR PANEL-->
		<div class="large-4 columns">
			<div class="sponsor-panel">
				<h2>2014 Sponsors</h2>	
				<div class="inside-panel">
					<a href="#"><img src="img/countdown-sponsor.png" alt="countdown-sponsor" width="279" height="96" /></a>
				</div>
			</div>
		</div>
		
		<!--START NEWSLETTER PANEL-->
		<div class="large-4 columns">
			<div class="newsletter-panel">
				<h2>Newsletter</h2>
				<div class="inside-panel">
					<!--START FORM-->
					<form>
					  <div class="row">
					    <div class="large-12 columns">
					      <div class="row collapse">
					      	<label>Sign Up for Our Newsletter!</label>
					        <div class="large-12 columns">
								<input type="text" placeholder="Email Address">
					        </div>
					      </div>
					    </div>
					  </div>
					  <div class="row">
						  <div class="large-6 columns large-centered">
							  <a class="button expand" href="#">Sign Up</a>
						  </div>
					  </div>
					</form>
				</div>
			</div>
		</div>
		
		<!--START SOCIAL MEDIA PANEL-->
		<div class="large-4 columns">
			<div class="social-panel">
				<h2>Social Media</h2>
				<div class="inside-panel">
					<p>CONNECT WITH US!</p>
					<ul class="inline-list">
						<li><a href="#">
							<img src="img/twitter.png" alt="twitter" />
						</a></li>
						<li><a href="#">
							<img src="img/facebook.png" alt="twitter" />
						</a></li>
						<li><a href="#">
							<img src="img/youtube.png" alt="twitter" />
						</a></li>
						<li><a href="#">
							<img src="img/instagram.png" alt="twitter" />
						</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
	include("footer.php");
?>