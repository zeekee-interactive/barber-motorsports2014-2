<!doctype html>
<html class="no-js" lang="en">
  <!-- <![endif] -->
  <head>
    <meta charset='utf-8'>
    <meta name="viewport" content="width=device-width" />
    <meta content='<?php echo $cMetaDesc; ?>' name='description'>
    <meta content='<?php echo $cMetaKW; ?>' name='keywords'>
    <title><?php echo $cPageTitle; ?> - Official Site of Honda Indy Grand Prix of Alabama</title>
     <link rel="stylesheet" href="stylesheets/app.css" />
     <link rel="stylesheet" href="stylesheets/jquery.countdown.css" />
    <script src="bower_components/modernizr/modernizr.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    
    <script type="text/javascript" src="//use.typekit.net/dss4iav.js"></script>
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
    	<!-- IE Fix for HTML5 Tags -->
    	<!--[if lt IE 9]>
      	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    	<![endif]-->
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-XXXXXX-Y']);
			_gaq.push(['_trackPageview']);
			
			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
    </script>
  </head>
  <body class="<?=$layout ?>">
<header class="contain-to-grid top-menu">
	<nav class="top-bar" data-topbar>
	  <ul class="title-area">
	    <li class="name">
	      <h1><a href="index.php"><img src="img/logo-1.png" alt="Barber Motorsports Park - Birmingham Alabama"></a></h1>
	    </li>
	    <li class="toggle-topbar menu-icon"><a href="#">Menu</a></li>
	  </ul>
	
	  <section class="top-bar-section">
	    <!-- Right Nav Section -->
	    <ul class="right">
	      <li><a href="#">Honda Indy Grand Prix<br class="show-for-medium-only" /> of Alabama</a></li>
	      <li><a href="#">Triumph SuperBike<br class="show-for-medium-only" /> Classic</a></li>
	      <li><a href="#">Barber Vintage<br class="show-for-medium-only" /> Festival</a></li>
	      <li class="has-dropdown"><a href="#">Additional<br class="show-for-medium-only" /> Events</a>
	      	<ul class="dropdown">
		      	<li><a href="#">LeMons</a></li>
		      	<li><a href="#">NASA</a></li>
		      	<li><a href="#">WERA</a></li>
		      	<li><a href="#">SCCA</a></li>
		      	<li><a href="#">Track Days</a></li>
		      	<li><a href="#">2014 Calendar</a></li>
	      	</ul>
	      </li>
	      <li class="has-dropdown"><a href="#">Park Info</a>
	        <ul class="dropdown">
		      	<li><a href="#">Directions</a></li>
		      	<li><a href="#">Park Rules &amp; Camping</a></li>
		      	<li><a href="#">Lodging &amp; Accommodations</a></li>
		      	<li><a href="#">Track Maps</a></li>
		      	<li><a href="#">Barber Vintage Motorsports Museum</a></li>
		      	<li><a href="#">Porsche Sport Driving School</a></li>
		      	<li><a href="#">Multimedia</a></li>
		      	<li><a href="#">Media Requests</a></li>
		      	<li><a href="#">Contact Us</a></li>
	      	</ul>
	      </li>
	    </ul>
	  </section>
	</nav>
</header>
  	