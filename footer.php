<!--
<div class="sitemap-div">
	<div class="row">
		<div class="large-3 columns right">
			<div class="sitemap-toggle">
				<h3><a href="#">Sitemap <span class="plus-sign"><img src="img/plus-sign.png" alt="plus-sign" width="22%;" /></span></a></h3>
			</div>
		</div>
	</div>
</div>
-->
<footer>
	<div class="footer-wrapper">
		<div class="row">
			<div class="large-9 columns">
				<div class="row">
					<div class="large-3 columns">
						<a href="index.php"><img src="img/logo-1.png" alt="logo-1" width="159" height="37" /></a>
					</div>
					<div class="large-9 columns">
						<p>Copyright &copy; <?php echo date("Y");?> Barber Motorsports Park</p>
					</div>
				</div>
			</div>
			<div class="large-3 columns">
				<a href="http://zeekeeinteractive.com/" target="_blank"><img src="img/zeekee-slug-white-2011.png" alt="zeekee-slug-white-2011" width="120" height="19" /></a>
			</div>
		</div>
	</div>
</footer>

<script src="bower_components/jquery/jquery.js"></script>
<script src="bower_components/foundation/js/foundation.min.js"></script>
<script src="js/app.js"></script>
<script src="js/jquery.countdown.js"></script>
    <script src="js/script.js"></script>
</body>
</html>